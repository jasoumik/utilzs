<?php

namespace App\Http\Controllers;

use App\Pdf;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.pdf.pdfhome');
    }
    public function pdfToDocx()
    {
        return view('pages.pdf.pdf_to_docx');
    }
    public function docxToPdf()
    {
        return view('pages.pdf.docx_to_pdf');
    }
    public function imgToPdf()
    {
        return view('pages.pdf.img_to_pdf');
    }
    public function pdfToText()
    {
        return view('pages.pdf.pdf_to_text');
    }
    public function pdfToJpg()
    {
        return view('pages.pdf.pdf_to_jpg');
    }
    public function pdfToPng()
    {
        return view('pages.pdf.pdf_to_png');
    }
    public function pdfCompressor()
    {
        return view('pages.pdf.pdf_compressor');
    }
    public function combinePdf()
    {
        return view('pages.pdf.combine_pdf');
    }
    public function extractPdf()
    {
        return view('pages.pdf.extract_pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pdf  $pdf
     * @return \Illuminate\Http\Response
     */
    public function show(Pdf $pdf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pdf  $pdf
     * @return \Illuminate\Http\Response
     */
    public function edit(Pdf $pdf)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pdf  $pdf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pdf $pdf)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pdf  $pdf
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pdf $pdf)
    {
        //
    }
}
