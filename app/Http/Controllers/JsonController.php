<?php

namespace App\Http\Controllers;

use App\Json;
use Illuminate\Http\Request;

class JsonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Json  $json
     * @return \Illuminate\Http\Response
     */
    public function show(Json $json)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Json  $json
     * @return \Illuminate\Http\Response
     */
    public function edit(Json $json)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Json  $json
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Json $json)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Json  $json
     * @return \Illuminate\Http\Response
     */
    public function destroy(Json $json)
    {
        //
    }
}
