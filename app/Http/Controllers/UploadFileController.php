<?php

namespace App\Http\Controllers;

use App\UploadFile;
use Illuminate\Http\Request;
use Buglinjo\LaravelWebp\Facades\Webp;

class UploadFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.pdf.pdf_to_docx');
    }

    public function fileUpload(Request $request){

        // if($request->hasFile('file')) {
   
        //   // Upload path
        //   $destinationPath = 'files/';
   
        //   // Create directory if not exists
        //   if (!file_exists($destinationPath)) {
        //      mkdir($destinationPath, 0755, true);
        //   }
   
        //   // Get file extension
        //   $extension = $request->file('file')->getClientOriginalExtension();
   
        //   // Valid extensions
        //   $validextensions = array("jpeg","jpg","png","pdf","docx");
   
        //   // Check extension
        //   if(in_array(strtolower($extension), $validextensions)){
   
        //     // Rename file 
        //     $fileName = str_slug(Carbon::now()->toDayDateTimeString()).rand(11111, 99999) .'.' . $extension;
   
        //     // Uploading file to given path
        //     $request->file('file')->move($destinationPath, $fileName); 
        //    // $docx = new CreateDocx();
        //   //  $docx->transformDocument($fileName , 'output.docx', 'msword');
        //   $webp = Webp::make($request->file('file')); 
        //     $webp->save(public_path('output.webp'));

        //   }
   
        // }
        $image = $request->file('file');

     $imageName = time() . '.' . $image->extension();

     $image->move(public_path('images'), $imageName);
     $webp = Webp::make($request->file('file')); 
     //dd($webp);
     $path = "public/images/";
        $webp->save($path.'output.webp');
     return response()->json(['success' => $imageName]);
     }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UploadFile  $uploadFile
     * @return \Illuminate\Http\Response
     */
    public function show(UploadFile $uploadFile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UploadFile  $uploadFile
     * @return \Illuminate\Http\Response
     */
    public function edit(UploadFile $uploadFile)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UploadFile  $uploadFile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UploadFile $uploadFile)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UploadFile  $uploadFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(UploadFile $uploadFile)
    {
        //
    }
}
