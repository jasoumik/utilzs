<?php

namespace App\Http\Controllers\OCR;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class OcrController extends Controller
{
    //Index Page
    public function index()
    {
        $title = config('app.name')."-OCR";
        return view('frontend.ocr.index',compact('title'));
    }
}
