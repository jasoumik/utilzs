<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('ocr','OCR\OcrController@index');

Route::get('/main','HomeController@index');
Route::get('/pdf-and-docx-tools','PdfController@index');
Route::get('/pdf_to_docx','PdfController@pdfToDocx');
Route::get('/docx_to_pdf','PdfController@docxToPdf');
Route::get('/img_to_pdf','PdfController@imgToPdf');
Route::get('/pdf_to_text','PdfController@pdfToText');
Route::get('/pdf_to_jpg','PdfController@pdfToJpg');
Route::get('/pdf_to_png','PdfController@pdfToPng');
Route::get('/pdf_compressor','PdfController@pdfCompressor');
Route::get('/combine_pdf','PdfController@combinePdf');
Route::get('/extract_pdf','PdfController@extractPdf');
Route::post('/upload_file','UploadFileController@fileUpload')->name('users.fileupload');