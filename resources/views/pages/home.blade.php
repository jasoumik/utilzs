@extends('layouts.master')
@section('content')
<div class="container-fluid">
        <style>
           th, td {
          text-align: center;
          
        }
      
.center {
  display: flex;
  justify-content: center;
  align-items: center;
  height: 200px;
  border: 3px solid gray; 
}

        </style>
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-30">
                        <div class="card-body">
        
                            <h3 class="mt-0 page-title text-center my_font" >Utilzs</h3>
                            <div class="center">
                            <div class="btn-group">
                            <a href="/pdf-and-docx-tools"><button class="btn btn-secondary btn-lg" type="button">
                             PDF & DOCX Tools
                            </button></a>
                            <button type="button" class="btn btn-lg btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="/pdf_to_docx">PDF to DOCX</a>
                            <a class="dropdown-item" href="/docx_to_pdf">DOCX to PDF</a>
                            <a class="dropdown-item" href="/img_to_pdf">Image to PDF</a>
                            <a class="dropdown-item" href="/pdf_to_text">PDF to Text</a>
                            <a class="dropdown-item" href="/pdf_to_jpg">PDF to JPG</a>
                            <a class="dropdown-item" href="/pdf_to_png">PDF to PNG</a>
                           
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/pdf_compressor">PDF Compressor</a>
                            <a class="dropdown-item" href="/combine_pdf">Combine PDF</a>
                            <a class="dropdown-item" href="/extract_pdf">Extract PDF</a>
                            </div>
                          </div>
                          &nbsp;&nbsp;
                          <div class="btn-group">
                            <a href=""><button class="btn btn-secondary btn-lg" type="button">
                              Image Tools
                            </button></a>
                            <button type="button" class="btn btn-lg btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Image to PDF</a>
                            <a class="dropdown-item" href="#">PDF to IMAGE</a>
                            <a class="dropdown-item" href="#">JPG to PNG</a>
                            <a class="dropdown-item" href="#">PNG to JPG</a>
                            <a class="dropdown-item" href="#">JPG to WEBP</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Image to PPTX</a>
                            <a class="dropdown-item" href="#">PPTX to Image</a>
                            <a class="dropdown-item" href="#">Image Size Compression</a>
                            </div>
                          </div>
                          &nbsp;&nbsp;
                          <div class="btn-group">
                            <button class="btn btn-secondary btn-lg" type="button">
                              JSON Converter
                            </button>
                            <button type="button" class="btn btn-lg btn-secondary dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <a class="dropdown-item" href="#">Something else here</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Separated link</a>
                            </div>
                          </div>
                          </div>

                          <div class="row justify-content-between align-items-center">
                            <div class="col-md-4 d-flex align-items-center justify-content-between justify-content-md-start mb-3 mb-md-0">
                              <div class="align-items-center m-4 d-inline-flex">
                              <!-- <a class="btn btn-purple form-control" href=""  id="createProject"> -->
                             
                              </div>
                              
                              

                            </div>
        
                            
                          </div>
                          <!-- validation error message -->
                                                        @if ($errors->any())
                                                        <div class="alert alert-danger">
                                                            <ul>
                                                                @foreach ($errors->all() as $error)
                                                                    <li>{{ $error }}</li>
                                                                @endforeach
                                                            </ul>
                                                        </div>
                                                         @endif
                          <!-- /.card-header -->
        
                            
        
                        </div>
                    </div>
            
 
        
        </div><!-- container -->
  
   
        </div>
        </div>
@endsection