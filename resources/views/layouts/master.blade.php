<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Project_Tracker') }}</title>

    <!-- Scripts -->
   

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<!-- CSS -->
<link rel="stylesheet" type="text/css" href="{{asset('dropzone/dist/min/dropzone.min.css')}}">

<!-- JS -->

    <!-- Styles -->
  
    
    

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>
        @yield('title')
    </title>
  
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    
   

 
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
   
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
   
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    
   <!-- jQuery  -->
   <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>

   <script src="{{ asset( 'assets/js/modernizr.min.js')}}"></script>
   <script src="{{ asset( 'assets/js/jquery.slimscroll.js')}}"></script>
   <script src="{{ asset( 'assets/js/waves.js')}}"></script>
   <script src="{{ asset( 'assets/js/jquery.nicescroll.js')}}"></script>
   <script src="{{ asset( 'assets/js/jquery.scrollTo.min.js')}}"></script>
   
   <!--Morris Chart-->
   <!-- <script src="{{ asset( 'assets/plugins/morris/morris.min.js')}}"></script> -->
   <script src="{{ asset( 'assets/plugins/raphael/raphael-min.js')}}"></script>
   
   <!-- <script src="{{ asset( 'assets/pages/dashboard.js')}}"></script> -->
   <!-- Required datatable js -->
   <script src="{{ asset( 'assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
   <script src="{{asset('dropzone/dist/min/dropzone.min.js')}}" type="text/javascript"></script>
   <!-- Buttons examples -->
   <script src="https://shaack.com/projekte/bootstrap-input-spinner/src/bootstrap-input-spinner.js"></script>
   
 
   <script src="{{ asset( 'assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/jszip.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/pdfmake.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/vfs_fonts.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/buttons.html5.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/buttons.print.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/buttons.colVis.min.js')}}"></script>
   <!-- Responsive examples -->
   <script src="{{ asset( 'assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
   <script src="{{ asset( 'assets/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>
   
   <!-- Datatable init js -->
   <script src="{{ asset( 'assets/pages/datatables.init.js')}}"></script>
   
   <!-- App js -->
   <script src="{{ asset( 'assets/js/app.js')}}"></script>
   <script src="https://unpkg.com/sweetalert@2.1.2/dist/sweetalert.min.js"></script>
   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  
    


    <!-- Basic Css files -->
   
    <link href="{{ asset( 'assets/css/icons.css' )}}" rel="stylesheet" type="text/css">
   

<link href="{{ asset( 'assets/plugins/datatables/dataTables.bootstrap4.min.css' )}}" rel="stylesheet" type="text/css" />

    <style>
        part_2{
          width: 50%;
        }
      </style>
</head>
<body>
    
        <main class="py-4">
            <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
            @yield('content')
        </main>
    </div>
</body>
</html>
